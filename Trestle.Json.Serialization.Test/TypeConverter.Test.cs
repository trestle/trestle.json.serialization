﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Trestle.Json.Serialization.Test
{
	[TestFixture]
	internal class TypeConverterTest
	{
		[Test]
		public void DuckTyping()
		{
			var formatter = new JsonFormatter
			{
				TypeConverter = new TypeConverter
				{
					DuckTypes = new Dictionary<Type, List<string>>
					{
						{ typeof(Widget),new List<string>{ "code|message" } },
						{typeof(Foo),new List<string>{"id"} },
						{typeof(Bar), new List<string>{"name","description"} }
					}
				}
			};
			var widget = formatter.Deserialize(GetStream("{ \"code\" : \"0\" }")) as Widget;
			Assert.NotNull(widget, nameof(widget));
			var foo = formatter.Deserialize(GetStream("{ \"id\" : \"1\" }")) as Foo;
			Assert.NotNull(foo, nameof(foo));
			var test = formatter.Deserialize(GetStream("{ \"name\" : \"a\" }")) as Bar;
			Assert.IsNull(test, nameof(test));
			var bar = formatter.Deserialize(GetStream("{ \"name\" : \"a\", \"description\" : \"a\" }")) as Bar;
			Assert.NotNull(bar, nameof(bar));

			formatter.TypeConverter.DuckTypes = null;
			widget = formatter.Deserialize(GetStream("{ \"code\" : \"0\" }")) as Widget;
			Assert.IsNull(widget, nameof(widget));
		}

		private static Stream GetStream(string value)
		{
			return new MemoryStream(Encoding.UTF8.GetBytes(value));
		}
	}
}