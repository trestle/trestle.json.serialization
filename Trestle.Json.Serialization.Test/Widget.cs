﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Trestle.Json.Serialization
{
	[Serializable]
	internal sealed class Widget : ISerializable
	{
		private Widget(SerializationInfo info, StreamingContext context)
		{
			if (info.Contains("code"))
			{
				Code = info.GetInt32("code");
			}
			if (info.Contains("message"))
			{
				Message = info.GetString("message");
			}
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (Code >= 0)
			{
				info.AddValue("code", Code);
			}

			if (Message.Length > 0)
			{
				info.AddValue("message", Message);
			}
		}

		public int Code { get; } = -1;
		public string Message { get; } = string.Empty;
	}
}