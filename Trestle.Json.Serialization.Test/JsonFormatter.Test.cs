using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Trestle.Json.Serialization
{
	[TestFixture]
	public static class JsonFormatterTest
	{
		[Test]
		public static void Deserialize_Serialize()
		{
			var stream = typeof(JsonFormatterTest).Assembly.GetManifestResourceStream(
				"Trestle.Json.Serialization.Test.JsonFormatter.Test.json");
			var formatter = new JsonFormatter();
			var data = formatter.Deserialize(stream) as IDictionary;
			Assert.NotNull(data, nameof(data));
			using (var memory = new MemoryStream())
			{
				formatter.Serialize(memory, data);
				memory.Seek(0, SeekOrigin.Begin);
				var data2 = formatter.Deserialize(memory) as IDictionary;

				Assert.NotNull(data2, nameof(data2));
			}
			Assert.IsNull(formatter.TypeConverter);

			using (var memory = new MemoryStream())
			{
				formatter.Serialize(memory, new Dictionary<string, object>
				{
					{"foo", new Foo() }
				});
			}
		}

		[Test]
		[TestCase("     ")]
		[TestCase("[x")]
		[TestCase("{")]
		[TestCase("[0,1")]
		public static void Exceptional_Condition(string data)
		{
			var formatter = new JsonFormatter();
			using (var memory = new MemoryStream(Encoding.UTF8.GetBytes(data)))
			{
				Assert.Throws<InvalidDataException>(() => formatter.Deserialize(memory));
			}
		}
	}
}