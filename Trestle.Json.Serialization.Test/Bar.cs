﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Trestle.Json.Serialization
{
	[Serializable]
	internal sealed class Bar : ISerializable
	{
		private Bar(SerializationInfo info, StreamingContext context)
		{
			if (info.Contains("name"))
			{
				Name = info.GetString("name");
			}
			if (info.Contains("description"))
			{
				Description = info.GetString("description");
			}
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (Name.Length > 0)
			{
				info.AddValue("name", Name);
			}

			if (Description.Length > 0)
			{
				info.AddValue("description", Description);
			}
		}

		public string Name { get; } = string.Empty;
		public string Description { get; } = string.Empty;
	}
}