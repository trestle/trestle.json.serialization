﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Trestle.Json.Serialization
{
	[Serializable]
	internal sealed class Foo : ISerializable
	{
		public Foo()
		{
			Id = "22";
		}

		private Foo(SerializationInfo info, StreamingContext context)
		{
			if (info.Contains("id"))
			{
				Id = info.GetString("id");
			}
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (Id.Length > 0)
			{
				info.AddValue("id", Id);
			}
		}

		public string Id { get; } = string.Empty;
	}
}