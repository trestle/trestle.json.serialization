NO_AUTO_COMMIT=true
VERSION='0.0.0'
require 'dev'

task :setup do
    Setup.setupStandardClassLib('Trestle.Json.Serialization','C#') if(!Dir.exists?('Trestle.Json.Serialization'))
	Dir.chdir('Trestle.Json.Serialization.Test') do
		puts `dotnet add package coverlet.msbuild`
	end

	yml_version=IO.read('.gitlab-ci.yml').scan(/\/v:"([\d.]+)"/)[0][0]
	puts "yml_version = #{yml_version}"
	Text.replace_in_file('.gitlab-ci.yml',"v:\"#{yml_version}\"","v:\"#{VERSION}\"")
end

task :build do
	puts `dotnet build --configuration Debug`
	puts `dotnet build --configuration Release`
end

task :test do
    puts `dotnet test Trestle.Json.Serialization.Test/Trestle.Json.Serialization.Test.csproj /p:CollectCoverage=true`
end

task :analyze => [:setup] do
	puts `dotnet sonarscanner begin /k:"trestle.json.serialization" /d:sonar.organization="trestle" /d:sonar.host.url="https://sonarcloud.io" /d:sonar.login="6a911abaee5679cf80093de7a4047197df5e692e" /d:sonar.cs.opencover.reportsPaths="Trestle.Json.Serialization.Test/coverage.opencover.xml" /v:"#{VERSION}"`
	puts `dotnet test Trestle.Json.Serialization.Test/Trestle.Json.Serialization.Test.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover`
	puts `dotnet sonarscanner end  /d:sonar.login="6a911abaee5679cf80093de7a4047197df5e692e"`
end

task :default