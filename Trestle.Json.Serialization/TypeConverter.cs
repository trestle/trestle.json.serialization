﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Trestle.Json.Serialization
{
	public sealed class TypeConverter
	{
		public object Convert(IDictionary dictionary)
		{
			if (DuckTypes is null)
			{
				return dictionary;
			}

			foreach (var type in DuckTypes.Keys)
			{
				if (KeysMatch(dictionary, DuckTypes[type]))
				{
					return dictionary.Create(type);
				}
			}

			return dictionary;
		}

		private static bool KeysMatch(IDictionary dictionary, List<string> duck_keys)
		{
			foreach (var key in duck_keys)
			{
				if (!HasMatch(dictionary, key))
				{
					return false;
				}
			}
			return true;
		}

		private static bool HasMatch(IDictionary dictionary, string duck_key)
		{
			if (duck_key.Contains("|"))
			{
				var keys = duck_key.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
				foreach (var subkey in keys)
				{
					if (dictionary.Contains(subkey))
					{
						return true;
					}
				}
			}
			else
			{
				if (dictionary.Contains(duck_key))
				{
					return true;
				}
			}
			return false;
		}

		public Dictionary<Type, List<string>> DuckTypes { get; set; } = new Dictionary<Type, List<string>>();
	}
}