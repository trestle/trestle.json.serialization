﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization;

namespace Trestle.Json.Serialization
{
	/// <summary>
	/// Extension methods for IDictionary
	/// </summary>
	internal static class IDictionaryExtension
	{
		/// <summary>
		/// Create an instance of specified type from an IDictionary instance.
		/// </summary>
		/// <param name="dictionary"></param>
		/// <param name="type"></param>
		/// <returns></returns>
		public static object Create(this IDictionary dictionary, Type type)
		{
			var serializationInfo = new SerializationInfo(type, new FormatterConverter());
			foreach (var key in dictionary.Keys)
			{
				serializationInfo.AddValue(key.ToString(), dictionary[key]);
			}
			var streamingContext = new StreamingContext();

			var paramTypes = new Type[] { typeof(SerializationInfo), typeof(StreamingContext) };
			ConstructorInfo ci = type.GetConstructor(
									BindingFlags.Instance | BindingFlags.NonPublic,
									null, paramTypes, null);

			return ci.Invoke(new object[] { serializationInfo, streamingContext });
		}
	}
}