﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Trestle.Json.Serialization
{
	public static class SerializationInfoExtension
	{
		public static IDictionary GetPersistentData(this SerializationInfo info)
		{
			var data = new Dictionary<string, object>();
			SerializationInfoEnumerator e = info.GetEnumerator();
			while (e.MoveNext())
			{
				data.Add(e.Name, e.Value);
			}
			return data;
		}

		public static bool Contains(this SerializationInfo info, string key)
		{
			SerializationInfoEnumerator e = info.GetEnumerator();
			while (e.MoveNext())
			{
				if (e.Name == key)
				{
					return true;
				}
			}
			return false;
		}
	}
}