﻿using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace Trestle.Json.Serialization
{
	internal class JsonWriter
	{
		public void Write(Stream stream, object value)
		{
			using (StreamWriter writer = new StreamWriter(stream, Encoding.Default, 1024, true))
			{
				Write(writer, value);
			}
		}

		private void Write(TextWriter writer, object value)
		{
			if (value is null)
			{
				writer.Write("null");
			}
			else if (value is string)
			{
				WriteString(writer, value as string);
			}
			else if (value is IDictionary)
			{
				WriteIDictionary(writer, value as IDictionary);
			}
			else if (value is IEnumerable)
			{
				WriteIEnumerable(writer, value as IEnumerable);
			}
			else if (value is ISerializable)
			{
				WriteISerializable(writer, value as ISerializable);
			}
			else if (value is bool)
			{
				writer.Write(value.ToString().ToLower());
			}
			else if (value is int || value is long || value is uint
					|| value is float || value is double)
			{
				writer.Write(value);
			}
		}

		private static void WriteString(TextWriter writer, string value)
		{
			// Escape '\' first
			var escaped_value = value.Replace("\\", "\\\\");
			escaped_value = escaped_value.Replace("\"", "\\\"");
			writer.Write($"\"{escaped_value}\"");
		}

		private void WriteIEnumerable(TextWriter writer, IEnumerable enumerable)
		{
			writer.Write("[");
			var itemCount = 0;
			foreach (object item in enumerable)
			{
				if (itemCount > 0)
				{
					writer.Write(",");
				}

				Write(writer, item);
				itemCount++;
			}
			writer.Write("]");
		}

		private void WriteIDictionary(TextWriter writer, IDictionary dictionary)
		{
			writer.Write("{");
			var itemCount = 0;
			foreach (var key in dictionary.Keys)
			{
				if (itemCount > 0)
				{
					writer.Write(",");
				}

				WriteString(writer, key.ToString());
				writer.Write(":");
				Write(writer, dictionary[key]);
				++itemCount;
			}
			writer.Write("}");
		}

		private void WriteISerializable(TextWriter writer, ISerializable serializable)
		{
			WriteIDictionary(writer, serializable.GetPersistentData());
		}
	}
}