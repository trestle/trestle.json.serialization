﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Trestle.Json.Serialization
{
	internal class JsonReader
	{
		public Serialization.TypeConverter TypeConverter { get; set; }

		public object Read(Stream stream)
		{
			var buffer = new BufferedStream(stream);
			using (TextReader reader = new StreamReader(buffer, Encoding.Default, true, 1024, true))
			{
				return Read(reader);
			}
		}

		private object Read(TextReader reader)
		{
			reader.EatWhiteSpace();
			var ichar = reader.Peek();
			if (ichar < 0)
			{
				throw new InvalidDataException("end of stream reached");
			}

			var c = (char)ichar;
			// char type 'n' null '\d' number '"' or '\'' string 'f' or 't' bool '{' object (hash)
			// '[' array
			if (c == '"')
			{
				return ReadString(reader);
			}

			if (c == '{')
			{
				return ReadObject(reader);
			}

			if (c == '[')
			{
				return ReadArray(reader);
			}

			if (c == 'f' || c == 't')
			{
				return ReadBool(reader);
			}

			if (c == 'n')
			{
				return ReadNull(reader);
			}

			return ReadNumber(reader);
		}

		private const string unicodeDoubleQuotes = @"\u0022";
		private const string doubleQuotes = @"""";
		private const string unicodeBackslash = @"\u005c";
		private const string backslash = @"\";

		private static string ReadString(TextReader reader)
		{
			reader.EatWhiteSpace();
			var stringResult = reader.SeekIgnoreEscaped((char)reader.Read());
			stringResult = stringResult.Replace("\\\\", "\\").Replace("\\\\", "\\");

			var result = stringResult.Replace(unicodeDoubleQuotes, doubleQuotes).Replace(unicodeBackslash, backslash);
			return result;
		}

		private static object ReadNumber(System.IO.TextReader reader)
		{
			reader.EatWhiteSpace();
			char[] endchars = { '}', ']', ',', ' ' };
			var nstr = reader.Seek(endchars);
			if (nstr.Contains("."))
			{
				var value = Convert.ToDouble(nstr);
				if (value <= Single.MaxValue)
				{
					return Convert.ToSingle(nstr);
				}

				return value;
			}
			else
			{
				var value = Convert.ToInt64(nstr);
				if (value <= Int32.MaxValue)
				{
					return Convert.ToInt32(nstr);
				}

				return value;
			}
		}

		private static object ReadNull(System.IO.TextReader reader)
		{
			reader.EatWhiteSpace();
			var ch = (char)reader.Peek();
			if (ch == 'n')
			{
				reader.Read(); reader.Read(); reader.Read(); reader.Read(); // read chars n,u,l,l
			}
			return null;
		}

		private static bool ReadBool(System.IO.TextReader reader)
		{
			reader.EatWhiteSpace();
			var ch = (char)reader.Peek();
			if (ch == 't')
			{
				reader.Read(); reader.Read(); reader.Read(); reader.Read(); // read chars t,r,u,e
				return true;
			}
			reader.Read(); reader.Read(); reader.Read(); reader.Read(); reader.Read(); // read char f,a,l,s,e
			return false;
		}

		private object ReadArray(TextReader reader)
		{
			var list = new List<object>();
			reader.Seek(new char[] { '[' });
			char ch;
			reader.Read(); // consume the '['
			reader.EatWhiteSpace();
			var done = false;
			ch = (char)reader.Peek();
			if (ch == ']')
			{
				done = true;
				reader.Read(); // consume the ']'
			}
			else
			{
				if (ch != 't' && ch != 'f' && ch != 'n' && Char.IsLetter(ch))
				{
					throw new InvalidDataException($"LoadArray char {ch} is not allowed after [");
				}
			}

			while (!done)
			{
				reader.EatWhiteSpace();
				list.Add(Read(reader));
				reader.EatWhiteSpace();
				ch = (char)reader.Peek();
				if (ch == ',')
				{
					reader.Read(); // consume ','
				}

				reader.EatWhiteSpace();
				ch = (char)reader.Peek();
				if (ch == ']')
				{
					reader.Read(); // consume ']'
					done = true;
				}
			}
			return list;
		}

		private object ReadObject(TextReader reader)
		{
			IDictionary dictionary = new Dictionary<string, object>();
			reader.Seek(new char[] { '{' });
			reader.Read(); // consume the '{'
			reader.EatWhiteSpace();
			var done = false;
			if ((char)(reader.Peek()) == '}')
			{
				done = true;
				reader.Read(); // consume the '}'
			}
			while (!done)
			{
				reader.EatWhiteSpace();
				var key = ReadString(reader);
				reader.EatWhiteSpace();
				var ichar = reader.Peek();
				if (ichar == -1)
				{
					throw new InvalidDataException("end of stream reached");
				}
				else
				{
					reader.Read(); //consume ':'
					dictionary[key] = Read(reader);
					reader.EatWhiteSpace();
					var ch = (char)reader.Peek();
					if (ch == ',')
					{
						reader.Read(); // consume ','
					}

					reader.EatWhiteSpace();
					ch = (char)reader.Peek();
					if (ch == '}')
					{
						reader.Read();
						reader.EatWhiteSpace();
						done = true;
					}
				}
			}
			if (TypeConverter != null)
			{
				return TypeConverter.Convert(dictionary);
			}
			return dictionary;
		}
	}
}