﻿using System.Collections;
using System.Runtime.Serialization;

namespace Trestle.Json.Serialization
{
	/// <summary>
	/// Extension methos for ISerializable
	/// </summary>
	public static class ISerializableExtension
	{
		public static IDictionary GetPersistentData(this ISerializable serializable)
		{
			var info = new SerializationInfo(serializable.GetType(), new FormatterConverter());
			serializable.GetObjectData(info, new StreamingContext());
			return info.GetPersistentData();
		}
	}
}