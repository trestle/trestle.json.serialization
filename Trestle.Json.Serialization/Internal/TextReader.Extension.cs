﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Trestle.Json.Serialization
{
	/// <summary>
	/// Extension methods for TextReader class.
	/// </summary>
	internal static class TextReaderExtension
	{
		/// <summary>
		/// Eats the whitespace in the TextReader
		/// </summary>
		/// <param name="reader"></param>
		public static void EatWhiteSpace(this TextReader reader)
		{
			while (Char.IsWhiteSpace((char)(reader.Peek())))
			{
				reader.Read();
			}
		}

		/// <summary>
		/// Seeks next occurrence of character(s) specified in values.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public static string Seek(this TextReader reader, char[] values)
		{
			var builder = new StringBuilder();
			var peekChar = (char)reader.Peek();
			if (values.Contains(peekChar))
			{
				return string.Empty;
			}

			while (reader.Peek() > -1)
			{
				var ichar = reader.Read();
				var cchar = (char)ichar;
				peekChar = (char)reader.Peek();
				builder.Append(cchar);
				if (values.Contains(peekChar))
				{
					return builder.ToString();
				}
			}
			return builder.ToString();
		}

		public static string SeekIgnoreEscaped(this TextReader reader, char value)
		{
			var builder = new StringBuilder();
			var iChar = 0;
			char curChar;
			var lastChar = ' ';
			while (reader.Peek() > -1)
			{
				iChar = reader.Read();
				curChar = (char)iChar;
				if (lastChar == '\\')
				{
					if (curChar == value)
					{
						builder.Append(curChar);
					}
					else { builder.Append(lastChar); builder.Append(curChar); }
				}
				else
				{
					if (curChar == value) { return builder.ToString(); }
					if (curChar != '\\') { builder.Append(curChar); }
				}

				lastChar = (char)iChar;
			}
			return builder.ToString();
		}
	}
}