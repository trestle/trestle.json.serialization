﻿using System.IO;

namespace Trestle.Json.Serialization
{
	/// <summary>
	/// Serializes and Deserializes objects to/from the JSON format.
	/// </summary>
	public class JsonFormatter
	{
		/// <summary>
		/// Deserializes the data in a stream to an object
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public object Deserialize(Stream stream)
		{
			return _jsonReader.Read(stream);
		}

		/// <summary>
		/// Serializes an object as JSON into a stream
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="graph"></param>
		public void Serialize(Stream stream, object graph)
		{
			_jsonWriter.Write(stream, graph);
		}

		/// <summary>
		/// The TypeConverter manages the conversion of JSON data into specific types during deserialization.
		/// </summary>
		public TypeConverter TypeConverter { get { return _jsonReader.TypeConverter; } set { _jsonReader.TypeConverter = value; } }
		private readonly JsonReader _jsonReader = new JsonReader();
		private readonly JsonWriter _jsonWriter = new JsonWriter();
	}
}